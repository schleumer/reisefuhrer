package li.ues.reisefuhrer.data.collections

import scala.concurrent.ExecutionContext.Implicits.global
import li.ues.reisefuhrer._

import data._
import models._
import Context._
import JodaMapper._
import spray.json._
import DescriptorType._

import org.joda.time.DateTime

class Descriptors(tag: Tag) extends Table[Descriptor](tag, "descriptors") {
  implicit val descriptorTypeMapper = enumValueMapper(DescriptorType) 

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def descriptorType = column[DescriptorType]("descriptor_type")
  def name = column[String]("name")
  def meta = column[JsValue]("meta")

  def * = (id.?, descriptorType, name, meta) <> (Descriptor.tupled, Descriptor.unapply)
}

object Descriptors extends TableQuery(new Descriptors(_)) {
  def add(it: Descriptor) = process(this += it)
}

class Pings(tag: Tag) extends Table[Ping](tag, "pings") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def senderId = column[Int]("sender_id")
  def descriptorId = column[Int]("descriptor_id")
  def osId = column[Int]("os_id")
  def userId = column[Int]("user_id")
  def when = column[DateTime]("when")

  def * = (id.?, senderId, descriptorId, osId, userId, when) <> (Ping.tupled, Ping.unapply)
}

object Pings extends TableQuery(new Pings(_)) {
  def all = process(this.result)
}