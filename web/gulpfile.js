var gulp = require('gulp')
  , browserify = require('browserify')
  , watchify = require('watchify')
  , babelify = require('babelify')
  , source = require('vinyl-source-stream')
  , notify = require('gulp-notify')
  , watch = require('gulp-watch')
  , less = require('gulp-less')
  , plumber = require('gulp-plumber')
  , path = require('path')
  , R = require('ramda')
  , pack = require('./package.json')
  , frontendDependencies = pack['frontendDependencies'];


var vendorBundle = browserify(R.merge(watchify.args, {
  paths: ['./src/js/'],
  debug: false,
  poll: true
}));

frontendDependencies.forEach(function (lib) {
  vendorBundle.require(lib);
});

// TODO: FIX
gulp.task('browserify-app', function () {
  var appBundleArgs = R.merge(watchify.args, {
    entries: ['./src/javascript/app.js'],
    paths: ['./src/javascript/'],
    debug: false,
    extensions: ['.html', '.js']
  });
  var appBundle = watchify(browserify(appBundleArgs))
    .transform(babelify.configure({
      ignore: /(node_modules)/,
      stage: 0,
      loose: "all"
    }));

  frontendDependencies.forEach(function (lib) {
    appBundle.external(lib);
  });

  function bundle() {
    return appBundle
      .bundle()
      .on('error', function(err) {
        console.log(err.message);
      })
      .pipe(plumber())
      .pipe(source('app.js'))
      //.pipe(replace(/\$app-version\$/g, getVersion))
      .pipe(gulp.dest('./public/js'));
  }

  appBundle.on('update', function () {
    var updateStart = Date.now();

    console.log("watchify updated, recompiling");
    bundle()
      .on('end', function () {
        console.log('Complete!', (Date.now() - updateStart) + 'ms');
      });
  });

  return bundle();
});

gulp.task('browserify-vendor', function() {
  var vendorBundle = browserify(R.merge(watchify.args, {
    paths: ['./src/javascript/'],
    debug: false,
    poll: true
  }));
  
  frontendDependencies.forEach(function (lib) {
    vendorBundle.require(lib);
  });

  return vendorBundle.bundle()
    .pipe(source('vendor.js'))
    .pipe(gulp.dest('./public/js'));
});

gulp.task('less', function(){
  return gulp.src('./src/less/app.less')
    .pipe(less({
      paths: [path.join(__dirname, 'node_modules')]
    }))
    .on('error', notify.onError("Error compiling LESS!\n <%= error.message %>"))
    .pipe(gulp.dest('./public/css'))
    .pipe(notify({
      message: "LESS compiled!",
      onLast: true
    }));
});

gulp.task('copy-static', function() {
  return gulp.src('./src/static/**/*')
    .pipe(gulp.dest('./public'));
});

gulp.task('copy-static-watch', function() {
  return watch('src/static/**/*', ['copy-static'], function(){
    return gulp.start('copy-static');
  });
});

gulp.task('react-watch', function(){
  return watch('src/javascript/**/*.js', ['react'], function(){
    return gulp.start('react');
  });
});

gulp.task('less-watch', function(){
  return watch(['src/less/**/*.less', 'src/less/**/*.css'], ['less'], function(){
    return gulp.start('less');
  });
});

gulp.task('default', [
  'browserify-vendor',
  'browserify-app',
  'less',
  'less-watch',
  'copy-static',
  'copy-static-watch'
]);