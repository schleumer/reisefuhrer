import React from 'react';

import { Login } from '../layouts';
import { Row, Column, Form, TextInput, Panel, Text, Button } from '../components';

class Auth extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Auth';
    }
    submit(e) {
      var form = {
        username: this.refs.username.getValue(),
        password: this.refs.password.getValue(),
      };

      console.log(form);

      e.preventDefault();
    }
    render() {
        return (
          <Login ref="layout">
            <Row>
              <Column size={Column.from(4, 6, 12, 12)} offset={Column.offset(4, 3, 0, 0)}>
                <Panel header={<Text>Entre com suas credenciais</Text>}>
                  <Form onSubmit={this.submit.bind(this)}>
                    <Form.Group name="">
                      <TextInput label="Login" placeholder="Seu login" ref="username" />
                    </Form.Group>
                    <Form.Group>
                      <TextInput label="Senha" placeholder="Sua senha" ref="password" password={true} />
                    </Form.Group>
                    <Form.Footer gravity="right">
                      <Button text="Entrar" type={Button.Primary} submit={true}/>
                    </Form.Footer>
                  </Form>
                </Panel>
                <Panel header={<Text>Entre com suas credenciais</Text>}>
                  <Form onSubmit={this.submit.bind(this)}>
                    <Form.Group name="">
                      <TextInput label="Login" placeholder="Seu login" ref="username" />
                    </Form.Group>
                    <Form.Group>
                      <TextInput label="Senha" placeholder="Sua senha" ref="password" password={true} />
                    </Form.Group>
                    <Form.Footer gravity="right">
                      <Button text="Entrar" type={Button.Primary} submit={true}/>
                    </Form.Footer>
                  </Form>
                </Panel>
              </Column>
            </Row>
          </Login>
        );
    }
}

export default Auth;
