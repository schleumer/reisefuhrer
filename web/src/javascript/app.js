require( './boot' );

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link } from 'react-router'

import { Container, SecureRoute } from './components';
import C from './controllers';

class App extends React.Component {
  constructor( props ) {
    super( props );
    this.displayName = 'App';
  }
  render() {
    return (
      <Container>
        { this.props.children }
      </Container>
    );
  }
}

ReactDOM.render( (
  <Router>
    <Route component={ App } >
      <SecureRoute path="/" component={ C.Home } />
      <Route path="/login" component={ C.Auth } />
    </Route>
  </Router>
), document.getElementById("app") );
