import React from 'react';

import { Auth } from '../services';

class SecureComponent extends React.Component {
  static willTransitionTo(transition) {
    console.log("lel");
    if(!Auth.isLogged()) {
      transition.redirect('/login', {}, { 'nextPath': transition.path });
    }
  }

  constructor(props) {
    super(props);
    this.displayName = 'SecureComponent';
  }
}

export default SecureComponent;
