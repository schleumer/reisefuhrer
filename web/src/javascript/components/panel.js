import React from 'react';

class Panel extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Panel';
  }
  render() {
    // TODO stuffs
    var header = this.props.header;

    var className = [
      'panel', this.props.type ? `panel-${this.props.type}` : 'panel-default'
    ];

    if (this.props.className) {
      className.push(this.props.className);
    }

    var props = {className: className.join(' '), ...this.props};

    return (
      <div {...props}>
        <div className="panel-heading">
          {header}
        </div>
        <div className="panel-body">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Panel;
