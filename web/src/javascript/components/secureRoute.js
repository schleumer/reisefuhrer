// Dirty, dirty and dirty

import React from 'react';
const { string, bool, func } = React.PropTypes

import { Route } from 'react-router';

import { Auth } from '../services';

var requireAuth = (nextState, replaceState, callback) => {
  if(!Auth.isLogged()) {
    replaceState({ nextPathname: nextState.location.pathname }, '/login');
    callback();
  }
}

class SecureRoute extends Route {
  static createRouteFromReactElement(element) {
    const route = super.createRouteFromReactElement(element);

    route.onEnter = requireAuth;

    return route;
  }
}

export default SecureRoute;