import React from 'react';

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.displayName = 'Footer';
  }
  render() {
    var props = {...this.props}
    var style = {...props.style};
    if(this.props.gravity) {
      style.textAlign = this.props.gravity;
    }
    props.style = style;

   return (<div {...props}>{this.props.children}</div>);
  }
}

export default Footer;
